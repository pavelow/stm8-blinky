# STM8 Blinky

Hello world of STM8, with HSE initialisation.


## Compiling & uploading
- Require `en.stsw-stm8069.zip` from ST site for register definitions
	- Extract and apply patch from here https://github.com/gicking/STM8-SPL_SDCC_patch
- Need sdcc & stm8flash to be installed for compiling and uploading using stlinkv2
- Run `make` to compile and upload binary

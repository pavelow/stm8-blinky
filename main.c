#include <stm8s.h>	// Register definitions
#define STM8S003	// Interrupt Headers

void Delay(uint16_t nCount);	// Delay For N cycles
void InitClock();				// Initialise HSE clock
void InitGPIO();				// Initialise GPIO Pin 3

void main(void) {
	InitClock();
	InitGPIO();

	while (1) {
		GPIOD->ODR = (~GPIOD->ODR & (0x01 << 3)) | (GPIOD->ODR & ~(0x01 << 3)); // Toggle pin 3
		Delay(0x0F00);
	}
}

void InitGPIO() { // Setup PortD Pin3 as output, pp107, Reference Manual
	GPIOD->DDR |= 0x01 << 3; // Set pin 3 as output
	GPIOD->CR1 |= 0x01 << 3; // Set pin 3 as push-pull output
}

void InitClock() {
	CLK->PCKENR1 = 0x00; // Peripheral clock gating Off to TIM1, TIM2, TIM3, SPI, I2C, UART
	CLK->PCKENR2 = 0x00; // Peripheral clock gating Off to ADC, AWU

	// Setup Clock to Automatically switch to HSE pp83, Reference Manual
	CLK->SWCR |= CLK_SWCR_SWEN;		// Clock Switching enabled
	CLK->SWR = 0xB4; 				// Set HSE Clock in Switch Register

	CLK->CKDIVR |= 0x07; // CPU Clock Divider 1/128 pp86, Reference Manual
}

void Delay(uint16_t nCount) { // Delay for N cycles
	while (nCount != 0) {
		nCount--;
	}
}

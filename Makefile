all: main.ihx flash

main.ihx: main.c
	sdcc -lstm8 -mstm8 --opt-code-size --std-sdcc99 --nogcse --all-callee-saves --debug --verbose --stack-auto --fverbose-asm --float-reent --no-peep -I./ -I./STM8S_StdPeriph_Lib/Libraries/STM8S_StdPeriph_Driver/src -I./STM8S_StdPeriph_Lib/Libraries/STM8S_StdPeriph_Driver/inc -D STM8S003 ./main.c

flash: main.ihx
	stm8flash -c stlinkv2 -p stm8s003f3 -s flash -w main.ihx
